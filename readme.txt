To use the framework:
1. Run main from TheTester class
2. Run tests using gradle; command: clean test
3. Generate allure report; command: allure generate build/allure-results --clean -o build/reports/allure
4. Generated report can be found in build/reports/allure directory

Note: Generating report requires installed Allure Command Line Tool - https://github.com/etki/allure-cli