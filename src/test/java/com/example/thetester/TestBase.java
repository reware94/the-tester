package com.example.thetester;

import com.example.thetester.swaggerSpecification.PathItemDTO;
import com.example.thetester.swaggerSpecification.SwaggerDTO;
import com.example.thetester.helpers.FilesHelper;
import com.example.thetester.testDataGenerators.AdaptiveRandomData;
import com.example.thetester.testDataGenerators.RandomWithCorrectTypeaAndReusingData;
import com.example.thetester.testDataGenerators.RandomWithCorrectTypeData;
import com.example.thetester.testDataGenerators.RandomWithIncorrectTypeData;
import com.google.gson.Gson;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import io.qameta.allure.Step;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import static org.testng.internal.collections.Ints.asList;

public class TestBase {

    private FilesHelper filesHelper = new FilesHelper();
    protected RandomWithCorrectTypeData randomWithCorrectTypeData = new RandomWithCorrectTypeData();
    protected AdaptiveRandomData adaptiveRandomData = new AdaptiveRandomData();
    protected RandomWithCorrectTypeaAndReusingData randomWithCorrectTypeaAndReusingData = new RandomWithCorrectTypeaAndReusingData();
    protected RandomWithIncorrectTypeData randomWithIncorrectTypeData = new RandomWithIncorrectTypeData();

    protected static List<String> DTO_NAMES;
    protected static List<String> ENDPOINT_NAMES = new ArrayList<>();

    protected static final List<Integer> EXPECTED_STATUS_CODES = asList(200, 201, 202, 204);

    @BeforeClass
    protected void prepareData() throws IOException {
        String str = filesHelper.readFile("./src/test/resources/specifications/tictactoe.json");
        SwaggerDTO swaggerDTO = new Gson().fromJson(str, SwaggerDTO.class);

        String baseUri = "";
        List<String> protocol = swaggerDTO.getSchemes();
        if (protocol != null) {
            if (protocol.stream().anyMatch(p -> p.equals("https"))) {
                baseUri = baseUri.concat("https://");
            } else {
                baseUri = baseUri.concat("http://");
            }
        } else{
            baseUri = baseUri.concat("http://");
        }
        RestAssured.baseURI = baseUri + swaggerDTO.getHost();
        RestAssured.basePath = swaggerDTO.getBasePath();

        if (swaggerDTO.getDefinitions() != null) {
            DTO_NAMES = Arrays.stream(swaggerDTO.getDefinitions().keySet().toArray()).map(Object::toString).collect(Collectors.toList());
        } else {
            DTO_NAMES = Collections.emptyList();
        }

        Object[] paths = swaggerDTO.getPaths().keySet().toArray();
        List<PathItemDTO> pathItemDTOS = Arrays.stream(paths).map(path -> swaggerDTO.getPaths().get(path)).collect(Collectors.toList());

        IntStream.range(0, pathItemDTOS.size()).forEach(i -> {
            if (pathItemDTOS.get(i).getGet() != null) {
                String operationId = pathItemDTOS.get(i).getGet().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("get".concat(paths[i].toString().replace("/", "")));
                }
            }
            if (pathItemDTOS.get(i).getPost() != null) {
                String operationId = pathItemDTOS.get(i).getPost().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("post".concat(paths[i].toString().replace("/", "")));
                }
            }
            if (pathItemDTOS.get(i).getPut() != null) {
                String operationId = pathItemDTOS.get(i).getPut().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("put".concat(paths[i].toString().replace("/", "")));
                }
            }
            if (pathItemDTOS.get(i).getDelete() != null) {
                String operationId = pathItemDTOS.get(i).getDelete().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("delete".concat(paths[i].toString().replace("/", "")));
                }
            }
            if (pathItemDTOS.get(i).getPatch() != null) {
                String operationId = pathItemDTOS.get(i).getPatch().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("patch".concat(paths[i].toString().replace("/", "")));
                }
            }
            if (pathItemDTOS.get(i).getHead() != null) {
                String operationId = pathItemDTOS.get(i).getHead().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("head".concat(paths[i].toString().replace("/", "")));
                }
            }
            if (pathItemDTOS.get(i).getOptions() != null) {
                String operationId = pathItemDTOS.get(i).getOptions().getOperationId();
                if (operationId != null) {
                    ENDPOINT_NAMES.add(operationId);
                } else {
                    ENDPOINT_NAMES.add("options".concat(paths[i].toString().replace("/", "")));
                }
            }
        });
    }

    @DataProvider
    public Object[][] allEndpoints() {
        ArrayList<String[]> allProducts = new ArrayList<>();
        for (String endpointName : ENDPOINT_NAMES) {
            allProducts.add(new String[]{endpointName});
        }
        return allProducts.toArray(new String[allProducts.size()][]);
    }

    protected Class<?> loadEndpoint(String endpointName) {
        return filesHelper.loadCompiledClass(
                new File(String.format("src/main/java/com/example/thetester/generated/endpoints/%s.java", endpointName)),
                endpointName);
    }

    protected RequestSpecification[] setContentType(Class<?> loadedEndpoint, Object endpoint) throws
            IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object getConsumes = loadedEndpoint.getMethod("getConsumes").invoke(endpoint);
        String consumes;
        if (getConsumes == null || getConsumes.toString().contains("application/json")) {
            consumes = "application/json";
        } else {
            consumes = ((List<String>) getConsumes).get(0);
        }

        final RequestSpecification[] requestSpecification = {given()};
        requestSpecification[0] = requestSpecification[0].contentType(consumes);

        if (consumes.equals("multipart/form-data")) {
            requestSpecification[0].config(RestAssured.config().encoderConfig(
                    encoderConfig().encodeContentTypeAs("multipart/form-data", ContentType.TEXT)));
        }
        return requestSpecification;
    }

    protected Response getResponse(String method, String path, RequestSpecification requestSpecification) {
        Response response = null;
        switch (method) {
            case "get":
                response = requestSpecification.get(path).prettyPeek();
                break;
            case "post":
                response = requestSpecification.post(path).prettyPeek();
                break;
            case "put":
                response = requestSpecification.put(path).prettyPeek();
                break;
            case "delete":
                response = requestSpecification.delete(path).prettyPeek();
                break;
            case "patch":
                response = requestSpecification.patch(path).prettyPeek();
                break;
            case "head":
                response = requestSpecification.head(path).prettyPeek();
                break;
            case "options":
                response = requestSpecification.options(path).prettyPeek();
                break;
        }
        return response;
    }

    @Step("Used test data")
    protected void inputData(String parameterCategory, String name, String type, String value) {}

    @Step("Response")
    protected void response(int statusCode, String response) {}
}
