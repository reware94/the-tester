package com.example.thetester.tests;

import com.example.thetester.TestBase;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import io.qameta.allure.Description;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.internal.collections.Ints.asList;

public class RandomWithCorrectTypeTest extends TestBase {

    @Test(invocationCount = 10, dataProvider = "allEndpoints")
    @Description("Test with input data generated randomly with correct type")
    public void randomInputsWithCorrectType(String endpointName) throws
            IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        System.out.println("Loading endpoint");
        Class<?> loadedEndpoint = loadEndpoint(endpointName);
        Object endpoint = loadedEndpoint.newInstance();

        System.out.println("Getting method, path and content type");
        String method = (String) loadedEndpoint.getMethod("getMethod").invoke(endpoint);
        String path = (String) loadedEndpoint.getMethod("getPath").invoke(endpoint);
        final RequestSpecification[] requestSpecification = setContentType(loadedEndpoint, endpoint);

        System.out.println("Setting body parameters");
        Map<String, String> bodyParameters = (Map<String, String>) loadedEndpoint.getMethod("getBodyParameters").invoke(endpoint);
        setBodyParameters(requestSpecification, bodyParameters);

        System.out.println("Setting path parameters");
        Map<String, String> pathParameters = (Map<String, String>) loadedEndpoint.getMethod("getPathParameters").invoke(endpoint);
        setPathParameters(requestSpecification, pathParameters);

        System.out.println("Setting query parameters");
        Map<String, String> queryParameters = (Map<String, String>) loadedEndpoint.getMethod("getQueryParameters").invoke(endpoint);
        setQueryParameters(requestSpecification, queryParameters);

        System.out.println("Setting form parameters");
        Map<String, String> formParameters = (Map<String, String>) loadedEndpoint.getMethod("getFormParameters").invoke(endpoint);
        setFormParameters(requestSpecification, formParameters);

        System.out.println("Sending request");
        requestSpecification[0].log().all();
        Response response = getResponse(method, path, requestSpecification[0]);
        response.prettyPeek();
        response(response.getStatusCode(), response.prettyPrint());

        System.out.println("Checking status code");
        assertTrue(EXPECTED_STATUS_CODES.contains(response.statusCode()));
    }

    private void setBodyParameters(RequestSpecification[] requestSpecification, Map<String, String> bodyParameters) {
        if (bodyParameters != null) {
            Set<String> parameters = bodyParameters.keySet();
            parameters.forEach(parameter -> {
                Object body;
                boolean isArray = bodyParameters.get(parameter).equals("array");
                if (isArray) {
                    int numberOfElements = new Random().nextInt(10);
                    ArrayList<Object> listOfDtoInstances = new ArrayList<>();
                    for (int i = 0; i <= numberOfElements; i++) {
                        Object generatedValues = randomWithCorrectTypeData.generateValues(parameter, DTO_NAMES);
                        listOfDtoInstances.add(generatedValues);
                        inputData("body", parameter, parameter, generatedValues.toString());
                    }
                    body = listOfDtoInstances;
                } else {
                    body = randomWithCorrectTypeData.generateValues(parameter, DTO_NAMES);
                    inputData("body", parameter, parameter, body.toString());
                }
                requestSpecification[0] = requestSpecification[0].body(body);
            });
        }
    }

    private void setPathParameters(RequestSpecification[] requestSpecification, Map<String, String> pathParameters) {
        if (pathParameters != null) {
            Set<String> parameters = pathParameters.keySet();
            parameters.forEach(parameter -> {
                String typeOfParameter = pathParameters.get(parameter);
                Object value = randomWithCorrectTypeData.generateValues(typeOfParameter, DTO_NAMES);
                inputData("path", parameter, typeOfParameter, value.toString());
                requestSpecification[0] = requestSpecification[0].pathParam(parameter, value);
            });
        }
    }

    private void setQueryParameters(RequestSpecification[] requestSpecification, Map<String, String> queryParameters) {
        if (queryParameters != null) {
            Set<String> parameters = queryParameters.keySet();
            parameters.forEach(parameter -> {
                String typeOfParameter = queryParameters.get(parameter);
                Object value = randomWithCorrectTypeData.generateValues(typeOfParameter, DTO_NAMES);
                inputData("query", parameter, typeOfParameter, value.toString());
                requestSpecification[0] = requestSpecification[0].queryParam(parameter, value);
            });
        }
    }

    private void setFormParameters(RequestSpecification[] requestSpecification, Map<String, String> formParameters) {
        if (formParameters != null) {
            Set<String> parameters = formParameters.keySet();
            parameters.forEach(parameter -> {
                String typeOfParameter = formParameters.get(parameter);
                Object value = randomWithCorrectTypeData.generateValues(typeOfParameter, DTO_NAMES);
                inputData("form", parameter, typeOfParameter, value == null ? "null" : value.toString());
                requestSpecification[0] = requestSpecification[0].formParam(parameter, value);
            });
        }
    }
}
