package com.example.thetester.helpers;

import com.example.thetester.swaggerSpecification.DefinitionsDTO;
import com.example.thetester.swaggerSpecification.SchemaDTO;
import com.google.common.base.CaseFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DtoTemplateHelper {

    public Map<String, Object> prepareContentOfDTOFile(String dtoName, SchemaDTO schema) {
        Map<String, Object> data = new HashMap<>();
        data.put("className", dtoName);

        List<Map<String, String>> variables = new ArrayList<>();
        List<Map<String, Object>> constructorParameters = new ArrayList<>();
        List<Map<String, String>> constructorVariables = new ArrayList<>();
        List<Map<String, Object>> constructorParametersObject = new ArrayList<>();
        List<Map<String, String>> constructorVariablesObject = new ArrayList<>();
        String toStringParameters = "";

        DefinitionsDTO properties = schema.getProperties();
        Object[] parameters = properties.keySet().toArray();

        for (Object p : parameters) {
            String parameterType;
            if (properties.get(p.toString()).getRef() != null) {
                parameterType = properties.get(p.toString()).getRef().split("/")[2];
            } else {
                parameterType = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, properties.get(p.toString()).getType());
            }
            String parameterName = p.toString();
            String parameterNameCamelCase = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, p.toString());

            if (parameterType.equals("Array")) {
                if (properties.get(p.toString()).getItems().getType() != null) {
                    parameterType = String.format("List<%s>",
                            CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, properties.get(p.toString()).getItems().getType()));
                } else {
                    parameterType = String.format("List<%s>",
                            CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, properties.get(p.toString()).getItems().getRef().split("/")[2]));
                }
            }

            Map<String, String> variable = new HashMap<>();
            variable.put("parameterType", parameterType);
            variable.put("parameterName", parameterName);
            variable.put("parameterNameCamelCase", parameterNameCamelCase);
            variables.add(variable);

            Map<String, Object> constructorParameter = new HashMap<>();
            constructorParameter.put("parameterType", parameterType);
            constructorParameter.put("parameterName", parameterName);
            constructorParameter.put("comma", true);
            constructorParameters.add(constructorParameter);

            Map<String, String> constructorVariable = new HashMap<>();
            constructorVariable.put("parameterName", parameterName);
            constructorVariables.add(constructorVariable);

            Map<String, Object> constructorParameterObject = new HashMap<>();
            constructorParameterObject.put("parameterName", parameterName);
            constructorParameterObject.put("comma", true);
            constructorParametersObject.add(constructorParameterObject);

            Map<String, String> constructorVariableObject = new HashMap<>();
            constructorVariableObject.put("parameterName", parameterName);
            constructorVariableObject.put("parameterType", parameterType);
            constructorVariablesObject.add(constructorVariableObject);

            toStringParameters = toStringParameters.concat(String.format("\"%s: \" + %s + \"; \"\n + ", parameterName, parameterName));
        }
        constructorParameters.get(constructorParameters.size() - 1).replace("comma", false);
        constructorParametersObject.get(constructorParametersObject.size() - 1).replace("comma", false);

        data.put("variables", variables);
        data.put("constructorParameters", constructorParameters);
        data.put("constructorVariables", constructorVariables);
        data.put("constructorParametersObject", constructorParametersObject);
        data.put("constructorVariablesObject", constructorVariablesObject);
        data.put("toStringParameters", toStringParameters);

        return data;
    }
}
