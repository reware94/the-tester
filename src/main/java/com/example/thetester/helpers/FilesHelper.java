package com.example.thetester.helpers;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import javax.tools.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.nio.file.Files.move;
import static org.apache.commons.io.FileUtils.*;

public class FilesHelper {

    public String readFile(String path) throws IOException {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();
        return new String(data, "UTF-8");
    }

    public Template readAndCompileTemplate(String path) throws IOException {
        String template = readFile(path);
        return Mustache.compiler().escapeHTML(false).nullValue("null").compile(template);
    }

    public File createAndWriteFile(String directoryPath, Object fileName, String code) {
        File sourceFile;
        try {
            sourceFile = createTemporaryFile(directoryPath, fileName.toString());
            writeFile(sourceFile, code);
            return sourceFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private File createTemporaryFile(String directoryPath, String fileName) throws IOException {
        File initialFile = Files.createTempFile(Paths.get(directoryPath), "temp", ".java").toFile();
        File sourceFile = new File(initialFile.getParent() + String.format("/%s.java", fileName));
        initialFile.renameTo(sourceFile);
        return sourceFile;
    }

    private void writeFile(File sourceFile, String sourceCode) throws IOException {
        FileWriter writer = new FileWriter(sourceFile);
        writer.write(sourceCode);
        writer.close();
    }

    public void compileFiles(List<File> sourceFiles) throws IOException {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(sourceFiles.get(0).getParentFile()));
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(sourceFiles);
        compiler.getTask(null, fileManager, null, null, null, compilationUnits).call();
        fileManager.close();
    }

    public Class<?> loadCompiledClass(File sourceFile, String classname) {
        URLClassLoader classLoader = null;
        try {
            classLoader = new URLClassLoader(new URL[]{sourceFile.getParentFile().toURI().toURL()});
            return classLoader.loadClass(classname);
        } catch (MalformedURLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
