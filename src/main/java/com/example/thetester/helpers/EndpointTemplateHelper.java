package com.example.thetester.helpers;

import com.example.thetester.swaggerSpecification.*;
import com.samskivert.mustache.Template;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EndpointTemplateHelper {

    private FilesHelper filesHelper = new FilesHelper();


    public List<File> writeEndpointFiles(Template tmpl, String directoryPath, SwaggerDTO swaggerDTO) {
        List<File> sourceFiles = new ArrayList<>();
        PathsDTO paths = swaggerDTO.getPaths();
        Object[] pathNames = paths.keySet().toArray();
        for (Object pathName : pathNames) {
            PathItemDTO pathItemDTO = paths.get(pathName.toString());
            List<ParameterDTO> commonParameters = new ArrayList<>();
            if (pathItemDTO.getParameters() != null) {
                commonParameters.addAll(pathItemDTO.getParameters());
            }
            
            if (pathItemDTO.getGet() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getGet().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getGet().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "get", pathItemDTO.getGet(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getGet().getOperationId() == null ?
                        "get".concat(pathName.toString().replace("/", "")) : pathItemDTO.getGet().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
            if (pathItemDTO.getPost() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getPost().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getPost().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "post", pathItemDTO.getPost(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getPost().getOperationId() == null ?
                        "post".concat(pathName.toString().replace("/", "")) : pathItemDTO.getPost().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
            if (pathItemDTO.getPut() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getPut().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getPut().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "put", pathItemDTO.getPut(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getPut().getOperationId() == null ?
                        "put".concat(pathName.toString().replace("/", "")) : pathItemDTO.getPut().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
            if (pathItemDTO.getDelete() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getDelete().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getDelete().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "delete", pathItemDTO.getDelete(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getDelete().getOperationId() == null ?
                        "delete".concat(pathName.toString().replace("/", "")) : pathItemDTO.getDelete().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
            if (pathItemDTO.getPatch() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getPatch().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getPatch().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "patch", pathItemDTO.getPatch(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getPatch().getOperationId() == null ?
                        "patch".concat(pathName.toString().replace("/", "")) : pathItemDTO.getPatch().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
            if (pathItemDTO.getHead() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getHead().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getHead().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "head", pathItemDTO.getHead(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getHead().getOperationId() == null ?
                        "head".concat(pathName.toString().replace("/", "")) : pathItemDTO.getHead().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
            if (pathItemDTO.getOptions() != null) {
                List<ParameterDTO> parameters = new ArrayList<>(commonParameters);
                if (pathItemDTO.getOptions().getParameters() != null) {
                    parameters.addAll(pathItemDTO.getOptions().getParameters());
                }
                String code = tmpl.execute(prepareContentOfEndpointFile(pathName.toString(), "options", pathItemDTO.getOptions(), parameters));
                File sourceFile = filesHelper.createAndWriteFile(directoryPath, pathItemDTO.getOptions().getOperationId() == null ?
                        "options".concat(pathName.toString().replace("/", "")) : pathItemDTO.getOptions().getOperationId(), code);
                sourceFiles.add(sourceFile);
            }
        }
        return sourceFiles;
    }

    private Map<String, Object> prepareContentOfEndpointFile(String path, String method, OperationDTO operationDTO, List<ParameterDTO> parameters) {
        Map<String, Object> data = new HashMap<>();

        data.put("className", operationDTO.getOperationId() == null ? method.concat(path.replace("/", "")) : operationDTO.getOperationId());
        data.put("controllerName", operationDTO.getTags() == null ? "common" : operationDTO.getTags().get(0));
        data.put("path", path);
        data.put("method", method);

        String consumes = null;
        if (operationDTO.getConsumes() != null) {
            consumes = "asList(";
            for (int i = 0; i < operationDTO.getConsumes().size(); i++) {
                consumes = consumes.concat("\"" + operationDTO.getConsumes().get(i) + "\"");
                if (i != operationDTO.getConsumes().size() - 1) {
                    consumes = consumes.concat(", ");
                }
            }
            consumes = consumes.concat(")");
        }
        data.put("consumes", consumes);

        String produces = null;
        if (operationDTO.getProduces() != null) {
            produces = "asList(";
            for (int i = 0; i < operationDTO.getProduces().size(); i++) {
                produces = produces.concat("\"" + operationDTO.getProduces().get(i) + "\"");
                if (i != operationDTO.getProduces().size() - 1) {
                    produces = produces.concat(", ");
                }
            }
            produces = produces.concat(")");
        }
        data.put("produces", produces);

        if (!parameters.isEmpty()) {
            final String[] bodyParameters = {null};
            List<ParameterDTO> body = parameters.stream()
                    .filter(parameter -> parameter.getIn().equals("body")).collect(Collectors.toList());
            if (body.size() != 0) {
                bodyParameters[0] = "new HashMap<String, String>(){{\n";
                parameters.stream()
                        .filter(parameter -> parameter.getIn().equals("body")).forEach(q -> {
                    if (q.getSchema().getType() != null) {
                        String dto = q.getSchema().getItems().getRef().split("definitions/")[1];
                        bodyParameters[0] = bodyParameters[0].concat(String.format("put(\"%s\", \"array\");\n", dto));
                    } else {
                        String dto = q.getSchema().getRef().split("definitions/")[1];
                        bodyParameters[0] = bodyParameters[0].concat(String.format("put(\"%s\", \"single\");\n", dto));
                    }
                });
                bodyParameters[0] = bodyParameters[0].concat("}}");
            }
            data.put("bodyParameters", bodyParameters[0]);

            final String[] pathParameters = {null};
            List<ParameterDTO> paths = parameters.stream()
                    .filter(parameter -> parameter.getIn().equals("path")).collect(Collectors.toList());
            if (paths.size() != 0) {
                pathParameters[0] = "new HashMap<String, String>(){{\n";
                parameters.stream()
                        .filter(parameter -> parameter.getIn().equals("path"))
                        .forEach(pathParam -> {
                            String paramType = pathParam.getType().equals("array") ?
                                    String.format("List<%s>", pathParam.getItems().getType()) : pathParam.getType();
                            pathParameters[0] = pathParameters[0].concat(String.format("put(\"%s\", \"%s\");\n", pathParam.getName(), paramType));
                        });
                pathParameters[0] = pathParameters[0].concat("}}");
            }
            data.put("pathParameters", pathParameters[0]);

            final String[] queryParameters = {null};
            List<ParameterDTO> queries = parameters.stream()
                    .filter(parameter -> parameter.getIn().equals("query")).collect(Collectors.toList());
            if (queries.size() != 0) {
                queryParameters[0] = "new HashMap<String, String>(){{\n";
                parameters.stream()
                        .filter(parameter -> parameter.getIn().equals("query"))
                        .forEach(queryParam -> {
                            String queryType = queryParam.getType().equals("array") ?
                                    String.format("List<%s>", queryParam.getItems().getType()) : queryParam.getType();
                            queryParameters[0] = queryParameters[0].concat(String.format("put(\"%s\", \"%s\");\n", queryParam.getName(), queryType));
                        });
                queryParameters[0] = queryParameters[0].concat("}}");
            }
            data.put("queryParameters", queryParameters[0]);


            final String[] formParameters = {null};
            List<ParameterDTO> forms = parameters.stream()
                    .filter(parameter -> parameter.getIn().equals("formData")).collect(Collectors.toList());
            if (forms.size() != 0) {
                formParameters[0] = "new HashMap<String, String>(){{\n";
                parameters.stream()
                        .filter(parameter -> parameter.getIn().equals("formData"))
                        .forEach(formParam -> {
                            String formType = formParam.getType().equals("array") ?
                                    String.format("List<%s>", formParam.getItems().getType()) : formParam.getType();
                            formParameters[0] = formParameters[0].concat(String.format("put(\"%s\", \"%s\");\n", formParam.getName(), formType));
                        });
                formParameters[0] = formParameters[0].concat("}}");
            }
            data.put("formParameters", formParameters[0]);
        } else {
            data.put("bodyParameters", null);
            data.put("pathParameters", null);
            data.put("queryParameters", null);
            data.put("formParameters", null);
        }

        String expectedStatusCodes = null;
        if (operationDTO.getResponses() != null) {
            expectedStatusCodes = "asList(\"";
            for (int i = 0; i < operationDTO.getResponses().keySet().size(); i++) {
                expectedStatusCodes = expectedStatusCodes.concat(operationDTO.getResponses().keySet().toArray()[i].toString());
                if (i != operationDTO.getResponses().keySet().size() - 1) {
                    expectedStatusCodes = expectedStatusCodes.concat("\", \"");
                }
            }
            expectedStatusCodes = expectedStatusCodes.concat("\")");
        }
        data.put("expectedStatusCodes", expectedStatusCodes);

        return data;
    }
}
