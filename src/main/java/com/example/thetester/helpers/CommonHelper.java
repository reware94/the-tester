package com.example.thetester.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonHelper {

    public String getRegex(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        String trueType = null;
        if (matcher.find()) {
            trueType = matcher.group(0);
        }
        return trueType;
    }
}
