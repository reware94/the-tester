package com.example.thetester.templates;

public class TemplateDTO {
    private String exampleString;

    public String getExampleString() {
        return exampleString;
    }

    public void setExampleString(String exampleString) {
        this.exampleString = exampleString;
    }

    private Integer exampleInteger;

    public Integer getExampleInteger() {
        return exampleInteger;
    }

    public void setExampleInteger(Integer exampleInteger) {
        this.exampleInteger = exampleInteger;
    }

    public TemplateDTO() {}

    public TemplateDTO(String exampleString, Integer exampleInteger) {
        this.exampleString = exampleString;
        this.exampleInteger = exampleInteger;
    }
}
