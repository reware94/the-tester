package com.example.thetester;

import com.example.thetester.swaggerSpecification.SchemaDTO;
import com.example.thetester.swaggerSpecification.SwaggerDTO;
import com.example.thetester.helpers.DtoTemplateHelper;
import com.example.thetester.helpers.EndpointTemplateHelper;
import com.example.thetester.helpers.FilesHelper;
import com.google.gson.Gson;
import com.samskivert.mustache.Template;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TheTester {

    private static FilesHelper filesHelper = new FilesHelper();
    private static DtoTemplateHelper dtoTemplateHelper = new DtoTemplateHelper();
    private static EndpointTemplateHelper endpointTemplateHelper = new EndpointTemplateHelper();

    public static void main(String[] args) throws IOException {

        String str = filesHelper.readFile("./src/test/resources/specifications/tictactoe.json");
        SwaggerDTO swaggerDTO = new Gson().fromJson(str, SwaggerDTO.class);

        if (swaggerDTO.getDefinitions() != null) {
            prepareDTOs(swaggerDTO);
        }
        prepareEndpoints(swaggerDTO);

        System.out.println("Classes prepared!");
    }

    private static void prepareDTOs(SwaggerDTO swaggerDTO) throws IOException {
        Template tmpl = filesHelper.readAndCompileTemplate("./src/main/java/com/example/thetester/templates/TemplateDTO.mustache");

        String directoryPath = "./src/main/java/com/example/thetester/generated/dtos";

        File directory = new File(directoryPath);
        if (!directory.exists()){
            directory.mkdir();
        }

        List<File> sourceFiles = new ArrayList<>();
        Map<String, SchemaDTO> definitions = swaggerDTO.getDefinitions();

        Object[] names = definitions.keySet().toArray();

        for (Object name : names) {
            SchemaDTO schemaDTO = definitions.get(name.toString());
            String code = tmpl.execute(dtoTemplateHelper.prepareContentOfDTOFile(name.toString(), schemaDTO));
            File sourceFile = filesHelper.createAndWriteFile(directoryPath, name, code);
            sourceFiles.add(sourceFile);
        }

        filesHelper.compileFiles(sourceFiles);
    }

    private static void prepareEndpoints(SwaggerDTO swaggerDTO) throws IOException {
        Template tmpl = filesHelper.readAndCompileTemplate("./src/main/java/com/example/thetester/templates/TemplateEndpoint.mustache");
        String directoryPath = "./src/main/java/com/example/thetester/generated/endpoints";

        File directory = new File(directoryPath);
        if (!directory.exists()){
            directory.mkdir();
        }

        List<File> sourceFiles = endpointTemplateHelper.writeEndpointFiles(tmpl, directoryPath, swaggerDTO);

        filesHelper.compileFiles(sourceFiles);
    }
}
