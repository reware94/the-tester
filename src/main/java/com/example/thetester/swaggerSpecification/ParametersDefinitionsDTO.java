package com.example.thetester.swaggerSpecification;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
public class ParametersDefinitionsDTO extends LinkedHashMap<String, ParameterDTO> {
    private Map<String, Object> name;
}
