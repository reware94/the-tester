package com.example.thetester.swaggerSpecification;

import lombok.Data;

import java.util.List;

@Data
public class OperationDTO {
    private List<String> tags;
    private String summary;
    private String description;
    private ExternalDocumentationDTO externalDocs;
    private String operationId;
    private List<String> consumes;
    private List<String> produces;
    private List<ParameterDTO> parameters;
    private ResponsesDTO responses;
    private List<String> schemes;
    private Boolean deprecated;
    private List<SecurityRequirementDTO> security;
}
