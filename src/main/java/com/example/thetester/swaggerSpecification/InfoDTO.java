package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class InfoDTO {
    private String title;
    private String description;
    private String termsOfService;
    private ContactDTO contact;
    private LicenseDTO license;
    private String version;
}
