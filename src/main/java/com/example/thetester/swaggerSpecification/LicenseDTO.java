package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class LicenseDTO {
    private String name;
    private String url;
}
