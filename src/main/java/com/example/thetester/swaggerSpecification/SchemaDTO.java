package com.example.thetester.swaggerSpecification;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class SchemaDTO {
    @SerializedName("$ref")
    private String ref;
    private String format;
    private String title;
    private String description;
    @SerializedName("default")
    private Object _default;
    private double maximum;
    private Boolean exclusiveMaximum;
    private double minimum;
    private Boolean exclusiveMinimum;
    private Integer maxLength;
    private Integer minLength;
    private String pattern;
    private Integer maxItems;
    private Integer minItems;
    private Boolean uniqueItems;
    private Object maxProperties;
    private Object minProperties;
    private List<Boolean> required;
    @SerializedName("enum")
    private List<Object> _enum;
    private String type;
    private ItemsDTO items;
    private String allOf;
    private DefinitionsDTO properties;
    private SchemaDTO additionalProperties;
    private String discriminator;
    private Boolean readOnly;
    private XmlDTO xml;
    private ExternalDocumentationDTO externalDocs;
    private Object example;
}
