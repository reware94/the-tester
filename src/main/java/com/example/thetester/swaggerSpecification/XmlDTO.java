package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class XmlDTO {
    private String name;
    private String namespace;
    private String prefix;
    private Boolean attribute;
    private Boolean wrapper;
}
