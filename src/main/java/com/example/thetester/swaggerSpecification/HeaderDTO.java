package com.example.thetester.swaggerSpecification;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class HeaderDTO {
    private String description;
    private String type;
    private String format;
    private ItemsDTO items;
    private String collectionFormat;
    @SerializedName("default")
    private Object _default;
    private double maximum;
    private Boolean exclusiveMaximum;
    private double minimum;
    private Boolean exclusiveMinimum;
    private Integer maxLength;
    private Integer minLength;
    private String pattern;
    private Integer maxItems;
    private Integer minItems;
    private Boolean uniqueItems;
    @SerializedName("enum")
    private List<Object> _enum;
    private double multipleOf;
}
