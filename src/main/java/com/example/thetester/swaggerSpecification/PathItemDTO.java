package com.example.thetester.swaggerSpecification;

import lombok.Data;

import java.util.List;

@Data
public class PathItemDTO {
    private String $ref;
    private OperationDTO get;
    private OperationDTO put;
    private OperationDTO post;
    private OperationDTO delete;
    private OperationDTO options;
    private OperationDTO head;
    private OperationDTO patch;
    private List<ParameterDTO> parameters;
}
