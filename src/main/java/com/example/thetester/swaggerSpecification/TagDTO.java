package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class TagDTO {
    private String name;
    private String description;
    private ExternalDocumentationDTO externalDocs;
}
