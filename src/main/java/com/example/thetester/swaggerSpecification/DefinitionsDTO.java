package com.example.thetester.swaggerSpecification;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
public class DefinitionsDTO extends LinkedHashMap<String, SchemaDTO> {
    private Map<String, SchemaDTO> name;
}
