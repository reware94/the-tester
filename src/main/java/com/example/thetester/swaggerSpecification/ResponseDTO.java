package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class ResponseDTO {
    private String description;
    private SchemaDTO schema;
    private HeadersDTO headers;
    private ExampleDTO examples;
}
