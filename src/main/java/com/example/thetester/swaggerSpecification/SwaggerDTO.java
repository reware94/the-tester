package com.example.thetester.swaggerSpecification;

import lombok.Data;

import java.util.List;

@Data
public class SwaggerDTO {
    private String swagger;
    private InfoDTO info;
    private String host;
    private String basePath;
    private List<String> schemes;
    private List<String> consumes;
    private List<String> produces;
    private PathsDTO paths;
    private DefinitionsDTO definitions;
    private ParametersDefinitionsDTO parameters;
    private ResponsesDefinitionsDTO responses;
    private SecurityDefinitionsDTO securityDefinitions;
    private List<SecurityRequirementDTO> security;
    private List<TagDTO> tags;
    private ExternalDocumentationDTO externalDocs;
}
