package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class ExternalDocumentationDTO {
    private String description;
    private String url;
}
