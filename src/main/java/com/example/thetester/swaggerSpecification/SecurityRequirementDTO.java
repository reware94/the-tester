package com.example.thetester.swaggerSpecification;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
public class SecurityRequirementDTO extends LinkedHashMap<String, List<String>> {
    private Map<String, Object> name;
}
