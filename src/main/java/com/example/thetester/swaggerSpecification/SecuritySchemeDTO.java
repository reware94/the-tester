package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class SecuritySchemeDTO {
    private String type;
    private String description;
    private String name;
    private String in;
    private String flow;
    private String authorizationUrl;
    private String tokenUrl;
    private ScopesDTO scopes;
}
