package com.example.thetester.swaggerSpecification;

import lombok.Data;

@Data
public class ContactDTO {
    private String name;
    private String url;
    private String email;
}
