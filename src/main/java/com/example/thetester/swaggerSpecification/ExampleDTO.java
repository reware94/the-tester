package com.example.thetester.swaggerSpecification;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
public class ExampleDTO extends LinkedHashMap<String, Object> {
    private Map<String, Object> mime_type;
}
