package com.example.thetester.testDataGenerators;

import com.example.thetester.helpers.CommonHelper;
import com.example.thetester.helpers.FilesHelper;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class TestData {

    FilesHelper filesHelper = new FilesHelper();
    CommonHelper commonHelper = new CommonHelper();

    Object createNewDtoInstance(Class<?> dto, Object[] constructorParameters) {
        Constructor<?> constructorWithObjectsAsParameters = getConstructorWithObjectsAsParameters(dto);
        try {
            return constructorWithObjectsAsParameters.newInstance(constructorParameters);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Constructor<?> getConstructorWithObjectsAsParameters(Class<?> dto) {
        return Arrays.stream(dto.getConstructors())
                .filter(constructor -> constructor.getParameters().length != 0)
                .filter(constructor -> constructor.getParameterTypes()[0].toString().contains("Object"))
                .findFirst().get();
    }

    Constructor<?> getConstructorWithParameters(Class<?> dto) {
        return Arrays.stream(dto.getConstructors())
                .filter(constructor -> constructor.getParameters().length != 0)
                .filter(constructor -> !constructor.getParameterTypes()[0].toString().contains("Object"))
                .findFirst().get();
    }

    List<String> getParametersTypes(Parameter[] parameterTypes) {
        return Arrays.stream(parameterTypes)
                .map(parameterType -> parameterType.getParameterizedType().getTypeName()
                        .replace("java.util.", "")
                        .replace("java.lang.", "")
                        .replace("com.example.thetester.generated.dtos.", ""))
                .collect(Collectors.toList());
    }

    Class<?> getDto(String type) {
        return filesHelper.loadCompiledClass(new File(
                        String.format("src/main/java/com/example/thetester/generated/dtos/%s.java", type)),
                String.format("com.example.thetester.generated.dtos.%s", type));
    }
}
