package com.example.thetester.testDataGenerators;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Slf4j
public class RandomWithCorrectTypeData extends TestData {

    public Object generateValues(String type, List<String> dtoNames) {
        if (type.toLowerCase().contains("list")) {

            log.info("Extracting type of objects in the list");
            String trueType = commonHelper.getRegex(type, "(?<=<).*(?=>)");

            List<Object> returnValues = new ArrayList<>();
            int numberOfElements = new Random().nextInt(10);

            log.info("Check if the type is a DTO");
            Optional<String> optionalDTO = dtoNames.stream().filter(name -> name.toLowerCase().equals(trueType.toLowerCase())).findAny();

            if (optionalDTO.isPresent()) {
                log.info("List of DTOs: " + type);
                for (int i = 0; i <= numberOfElements; i++) {
                    returnValues.add(generateDto(trueType, dtoNames));
                }
            } else {
                log.info("List of primitive types: " + type);
                for (int i = 0; i <= numberOfElements; i++) {
                    returnValues.add(generateRandomCorrectType(trueType));
                }
            }
            return returnValues;
        } else {
            log.info("Checking if the type is a DTO");
            Optional<String> optionalDTO = dtoNames.stream().filter(name -> name.toLowerCase().equals(type.toLowerCase())).findAny();

            if (optionalDTO.isPresent()) {
                log.info("DTO: " + type);
                return generateDto(type, dtoNames);
            } else {
                log.info("Primitive type: " + type);
                return generateRandomCorrectType(type);
            }
        }
    }

    private Object generateDto(String type, List<String> dtoNames) {
        Class<?> dto = getDto(type);

        log.info("Getting constructors and constructor parameters");
        Constructor<?> constructorWithParameters = getConstructorWithParameters(dto);
        Parameter[] parameterTypes = constructorWithParameters.getParameters();
        List<String> parameterTypesProcessed = getParametersTypes(parameterTypes);
        Object[] constructorParameters = new Object[parameterTypes.length];

        log.info("Generating values for DTO constructor");
        for (int i = 0; i < parameterTypes.length; i++) {
            String param = parameterTypesProcessed.get(i);
            Object value = generateValues(param, dtoNames);
            constructorParameters[i] = value;
        }

        log.info("Creating new DTO instance");
        return createNewDtoInstance(dto, constructorParameters);
    }

    private Object generateRandomCorrectType(String type) {
        Object returnValue = null;
        switch (type.toLowerCase()) {
            case "string":
                returnValue = RandomStringUtils.randomAlphabetic(10);
                break;
            case "integer":
                returnValue = new Random().nextInt();
                break;
            case "number":
                double v = new Random().nextFloat() * (Math.pow(10, new Random().nextInt(7)));
                returnValue = new Random().nextBoolean() ? v : (v * -1);
                break;
            case "boolean":
                returnValue = new Random().nextBoolean();
                break;
        }
        log.info(type + ": " + returnValue);
        return returnValue;
    }
}
