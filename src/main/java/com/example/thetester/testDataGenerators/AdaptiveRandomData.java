package com.example.thetester.testDataGenerators;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Slf4j
public class AdaptiveRandomData extends TestData {

    private static final int POOL_SIZE = 1000;
    private ListMultimap<String, Integer> testInputsForIntegers = ArrayListMultimap.create();
    private ListMultimap<String, Double> testInputsForDoubles = ArrayListMultimap.create();
    private ListMultimap<String, Integer> testInputsForStrings = ArrayListMultimap.create();

    public Object generateValues(String type, List<String> dtoNames, String endpoint) {
        if (type.toLowerCase().contains("list")) {

            log.info("Extracting type of objects in the list");
            String trueType = commonHelper.getRegex(type, "(?<=<).*(?=>)");

            List<Object> returnValues = new ArrayList<>();
            int numberOfElements = new Random().nextInt(10);

            log.info("Check if the type is a DTO");
            Optional<String> optionalDTO = dtoNames.stream().filter(name -> name.toLowerCase().equals(trueType.toLowerCase())).findAny();

            if (optionalDTO.isPresent()) {
                log.info("List of DTOs: " + type);
                for (int i = 0; i <= numberOfElements; i++) {
                    returnValues.add(generateDto(trueType, dtoNames, endpoint));
                }
            } else {
                log.info("List of primitive types: " + type);
                for (int i = 0; i <= numberOfElements; i++) {
                    returnValues.add(generateRandomCorrectType(trueType, endpoint));
                }
            }
            return returnValues;
        } else {
            log.info("Checking if the type is a DTO");
            Optional<String> optionalDTO = dtoNames.stream().filter(name -> name.toLowerCase().equals(type.toLowerCase())).findAny();

            if (optionalDTO.isPresent()) {
                log.info("DTO: " + type);
                return generateDto(type, dtoNames, endpoint);
            } else {
                log.info("Primitive type: " + type);
                return generateRandomCorrectType(type, endpoint);
            }
        }
    }

    private Object generateDto(String type, List<String> dtoNames, String endpoint) {
        Class<?> dto = getDto(type);

        log.info("Getting constructors and constructor parameters");
        Constructor<?> constructorWithParameters = getConstructorWithParameters(dto);
        Parameter[] parameterTypes = constructorWithParameters.getParameters();
        List<String> parameterTypesProcessed = getParametersTypes(parameterTypes);
        Object[] constructorParameters = new Object[parameterTypes.length];

        log.info("Generating values for DTO constructor");
        for (int i = 0; i < parameterTypes.length; i++) {
            String param = parameterTypesProcessed.get(i);
            Object value = generateValues(param, dtoNames, endpoint);
            constructorParameters[i] = value;
        }

        log.info("Creating new DTO instance");
        return createNewDtoInstance(dto, constructorParameters);
    }

    private Object generateRandomCorrectType(String type, String endpoint) {
        Object returnValue = null;
        switch (type.toLowerCase()) {
            case "string":
                int stringLength = adaptiveRandomInputGenerationForStringLength(testInputsForStrings.get(endpoint));
                testInputsForStrings.put(endpoint, stringLength);
                returnValue = RandomStringUtils.randomAlphabetic(stringLength);
                break;
            case "integer":
                returnValue = adaptiveRandomInputGenerationForIntegers(testInputsForIntegers.get(endpoint));
                testInputsForIntegers.put(endpoint, (Integer) returnValue);
                break;
            case "number":
                returnValue = adaptiveRandomInputGenerationForDoubles(testInputsForDoubles.get(endpoint));
                testInputsForDoubles.put(endpoint, (Double) returnValue);
                break;
            case "boolean":
                returnValue = new Random().nextBoolean();
                break;
        }
        log.info(type + ": " + returnValue);
        return returnValue;
    }

    private int adaptiveRandomInputGenerationForIntegers(List<Integer> testInputs) {
        List<Integer> candidates = new ArrayList<>();
        for (int i = 0; i < POOL_SIZE; i++) {
            int v = new Random().nextInt(1000000);
            candidates.add(new Random().nextBoolean() ? v : (v * -1));
        }
        return adaptiveRandomInputGenerationForIntegers(testInputs, candidates);
    }

    private int adaptiveRandomInputGenerationForStringLength(List<Integer> testInputs) {
        List<Integer> candidates = new ArrayList<>();
        for (int i = 0; i < POOL_SIZE; i++) {
            candidates.add(new Random().nextInt(1000));
        }
        return adaptiveRandomInputGenerationForIntegers(testInputs, candidates);
    }

    private int adaptiveRandomInputGenerationForIntegers(List<Integer> testInputs, List<Integer> candidates) {
        int maxDistance = 0;
        int furthest = Integer.MIN_VALUE;
        for (Integer c : candidates) {
            int minDist = minDistance(c, testInputs);
            if (furthest == Integer.MIN_VALUE || minDist > maxDistance) {
                furthest = c;
                maxDistance = minDist;
            }
        }
        return furthest;
    }

    private int minDistance(int c, List<Integer> testInputs) {
        int minDistance = Integer.MAX_VALUE;
        for (Integer testInput : testInputs) {
            double euclideanDistance = Math.sqrt(Math.pow((c - testInput), 2));
            if (minDistance > euclideanDistance) {
                minDistance = (int) Math.round(euclideanDistance);
            }
        }
        return minDistance;
    }

    private double adaptiveRandomInputGenerationForDoubles(List<Double> testInputs) {
        List<Double> candidates = new ArrayList<>();
        for (int i = 0; i < POOL_SIZE; i++) {
            double generatedCandidate = new Random().nextFloat() * (Math.pow(10, new Random().nextInt(7)));
            candidates.add(generatedCandidate);
        }
        double maxDistance = 0;
        double furthest = Double.MIN_VALUE;
        for (Double c : candidates) {
            double minDist = minDistance(c, testInputs);
            if (furthest == Double.MAX_VALUE || minDist > maxDistance) {
                furthest = c;
                maxDistance = minDist;
            }
        }
        return furthest;
    }

    private double minDistance(double c, List<Double> testInputs) {
        double minDistance = Double.MAX_VALUE;
        for (Double testInput : testInputs) {
            double euclideanDistance = Math.sqrt(Math.pow((c - testInput), 2));
            if (minDistance > euclideanDistance) {
                minDistance = (int) euclideanDistance;
            }
        }
        return minDistance;
    }
}
