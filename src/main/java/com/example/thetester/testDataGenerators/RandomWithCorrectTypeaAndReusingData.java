package com.example.thetester.testDataGenerators;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;

@Slf4j
public class RandomWithCorrectTypeaAndReusingData extends TestData {

    private static List<String> strings;
    private static List<Integer> integers;
    private static List<Double> doubles;

    public void populateLists() {
        int noOfStrings = new Random().nextInt(10) + 1;
        strings = new ArrayList<>();
        IntStream.range(0, noOfStrings)
                .forEach(v -> strings.add(RandomStringUtils.randomAlphabetic(new Random().nextInt(11))));

        int noOfIntegers = new Random().nextInt(10) + 1;
        integers = new ArrayList<>();
        IntStream.range(0, noOfIntegers)
                .map(i -> new Random().nextInt(1000000))
                .forEach(v -> integers.add(new Random().nextBoolean() ? v : (v * -1)));

        int noOfDoubles = new Random().nextInt(10) + 1;
        doubles = new ArrayList<>();
        IntStream.range(0, noOfDoubles)
                .mapToDouble(i1 -> new Random().nextFloat() * (Math.pow(10, new Random().nextInt(7))))
                .forEach(v -> doubles.add(new Random().nextBoolean() ? v : (v * -1)));
    }

    public Object generateValues(String type, List<String> dtoNames) {
        if (type.toLowerCase().contains("list")) {

            log.info("Extracting type of objects in the list");
            String trueType = commonHelper.getRegex(type, "(?<=<).*(?=>)");

            List<Object> returnValues = new ArrayList<>();
            int numberOfElements = new Random().nextInt(10);

            log.info("Check if the type is a DTO");
            Optional<String> optionalDTO = dtoNames.stream().filter(name -> name.toLowerCase().equals(trueType.toLowerCase())).findAny();

            if (optionalDTO.isPresent()) {
                log.info("List of DTOs: " + type);
                for (int i = 0; i <= numberOfElements; i++) {
                    returnValues.add(generateDto(trueType, dtoNames));
                }
            } else {
                log.info("List of primitive types: " + type);
                for (int i = 0; i <= numberOfElements; i++) {
                    returnValues.add(generateRandomCorrectType(trueType));
                }
            }
            return returnValues;
        } else {
            log.info("Checking if the type is a DTO");
            Optional<String> optionalDTO = dtoNames.stream().filter(name -> name.toLowerCase().equals(type.toLowerCase())).findAny();

            if (optionalDTO.isPresent()) {
                log.info("DTO: " + type);
                return generateDto(type, dtoNames);
            } else {
                log.info("Primitive type: " + type);
                return generateRandomCorrectType(type);
            }
        }
    }

    private Object generateDto(String type, List<String> dtoNames) {
        Class<?> dto = getDto(type);

        log.info("Getting constructors and constructor parameters");
        Constructor<?> constructorWithParameters = getConstructorWithParameters(dto);
        Parameter[] parameterTypes = constructorWithParameters.getParameters();
        List<String> parameterTypesProcessed = getParametersTypes(parameterTypes);
        Object[] constructorParameters = new Object[parameterTypes.length];

        log.info("Generating values for DTO constructor");
        for (int i = 0; i < parameterTypes.length; i++) {
            String param = parameterTypesProcessed.get(i);
            Object value = generateValues(param, dtoNames);
            constructorParameters[i] = value;
        }

        log.info("Creating new DTO instance");
        return createNewDtoInstance(dto, constructorParameters);
    }

    private Object generateRandomCorrectType(String type) {
        Object returnValue = null;
        switch (type.toLowerCase()) {
            case "string":
                returnValue = strings.get(new Random().nextInt(strings.size()));
                break;
            case "integer":
                returnValue = integers.get(new Random().nextInt(integers.size()));
                break;
            case "number":
                returnValue = doubles.get(new Random().nextInt(doubles.size()));
                break;
            case "boolean":
                returnValue = new Random().nextBoolean();
                break;
        }
        log.info(type + ": " + returnValue);
        return returnValue;
    }
}
