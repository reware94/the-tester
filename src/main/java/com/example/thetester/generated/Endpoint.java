package com.example.thetester.generated;

import java.util.List;
import java.util.Map;

public interface Endpoint {

    String getControllerName();

    String getPath();

    String getMethod();

    List<String> getConsumes();

    List<String> getProduces();

    Map<String, String> getBodyParameters();

    Map<String, String> getPathParameters();

    Map<String, String> getQueryParameters();

    Map<String, String> getFormParameters();

    List<String> getExpectedStatusCodes();
}
